Feature: Tagged events

  Scenario: No tagged events
    Given there are some events:
      |title|start_at        |end_at          |tag_list|
      |notag|2043-01-01 15:00|2043-01-01 15:00|        |
      |blagh|2043-01-02 15:00|2043-01-02 15:00|        |
    When I am on the events page
    Then I should see the events
    And I should not see "Tags"

  Scenario: Some tagged events
    Given there are some events:
      |title|start_at        |end_at          |tag_list|
      |tagme|2043-01-01 15:00|2043-01-01 15:00|one,two |
      |blagh|2043-01-02 15:00|2043-01-02 15:00|two     |
    When I am on the events page
    Then I should see the events
    And I should see "Tags"
    And I should see a link to "one"
    And I should see a link to "two"

  Scenario: View tagged events
    Given there are some events:
      |title|start_at        |end_at          |tag_list|
      |tagme|2043-01-01 15:00|2043-01-01 15:00|one,two |
      |blagh|2043-01-02 15:00|2043-01-02 15:00|two     |
    When I am on the "one" tagged events page
    Then I should see a link to "tagme"
    And I should not see a link to "blagh"
    When I am on the "two" tagged events page
    Then I should see a link to "tagme"
    And I should see a link to "blagh"
