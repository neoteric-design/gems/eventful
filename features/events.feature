Feature: Events
  Create events
  List events

  Scenario: No events
    Given there are no events
    When I am on the events page
    Then I should see there are no events

  Scenario: No upcoming events
    Given there are no upcoming events
    When I am on the events page
    Then I should see there are no events

  Scenario: Only past events
    Given there are some past events
    When I am on the events page
    Then I should see there are no events

  Scenario: Some upcoming events
    Given there are some upcoming events
    When I am on the events page
    Then I should see linked titles of the events

  Scenario: Event detail page
    Given there is an event
    And I am on the event's detail page
    Then I should see the event's attributes:
      |title   |
      |start_at|
      |end_at  |
      |body_raw|
    And I should see the key image thumbnail

  Scenario: Events are sorted by nearest at the top
    Given there are some events:
      |title |start_at  |
      |later |2045-01-01|
      |sooner|2044-01-01|
    When I am on the events page
    Then I should see "sooner" first
    And I should see "later" second
