Given(/^there are some events:$/) do |table|
  @events = []

  table.hashes.each do |params|
    @events << Event.create!(params)
  end
end

Given(/^there are no events$/) do
  Event.destroy_all
end

Given(/^there are no upcoming events$/) do
  Event.upcoming.destroy_all
end

Given(/^there are some past events$/) do
  2.times do
    Event.create!(:title => 'My event!',
                  :start_at => DateTime.yesterday)
  end
end

Given(/^there are some upcoming events$/) do
  2.times do
    Event.create!(:title    => 'Please attend',
                  :start_at => DateTime.tomorrow)
  end
end

Given(/^there is an event$/) do
  image = ::Montage::Image.create!(
            :photo => File.open('features/support/uploads/image.jpg', 'rb')
          )

  @event = Event.create!(:title      => 'Hello, World',
                         :start_at   => DateTime.tomorrow,
                         :end_at     => DateTime.now.advance(:days => 2),
                         :body       => '<html>Supported!</html>',
                         :image_ids  => [image.id])
end

Given(/^I am on the event's detail page$/) do
  visit event_path(@event)
end

When(/^I am on the events page$/) do
  visit events_path
end

When(/^I am on the "(.*?)" tagged events page$/) do |tag_name|
  visit tagged_events_path(tag_name)
end

Then(/^I should see there are no events$/) do
  page.should have_content("There are no events")
end

Then(/^I should see linked titles of the events$/) do
  Event.find_each do |e|
    page.should have_link(e.title, :href => event_path(e))
  end
end

Then(/^I should see the event's attributes:$/) do |table|
  # table is a Cucumber::Ast::Table
  table.raw.flatten.each do |attr|
    value   = @event.send(attr.gsub('_raw',''))
    content = attr =~ /_raw$/ ? strip_tags(value) : value

    page.should have_content(content)
  end
end

Then(/^I should see the key image thumbnail$/) do
  page.should have_xpath("//img[@src=\"#{@event.key_image.url(:thumb)}\"]")
end

Then(/^I should see the events$/) do
  @events.each do |event|
    page.should have_link(event.title, :href => event_path(event))
  end
end

Then(/^I should see "(.*?)" first$/) do |event_title|
  page.first('.event').should have_link(event_title)
end

Then(/^I should see "(.*?)" second$/) do |event_title|
  page.all('.event')[1].should have_link(event_title)
end
