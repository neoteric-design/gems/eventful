When(/^show me the page$/) do
  save_and_open_page
end

Then(/^I should not see "(.*?)"$/) do |content|
  page.should_not have_content(content)
end

Then(/^I should see "(.*?)"$/) do |content|
  page.should have_content(content)
end

Then(/^I should see a link to "(.*?)"$/) do |text|
  page.should have_link(text)
end

Then(/^I should not see a link to "(.*?)"$/) do |text|
  page.should_not have_link(text)
end
