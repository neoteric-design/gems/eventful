module Eventful
  module FormattingHelper
    def date_range(event)
      starts = event.starts_at
      ends   = event.ends_at

      return if starts.blank?

      time = starts.strftime("%A, #{month_and_day(starts)}")

      time << middle_parts(starts, ends)
      time << ends.strftime(', %Y')
    end

    def time_range(event)
      format = '%l:%M%P'
      starts = event.starts_at
      ends   = event.ends_at

      return if starts.blank?

      time = starts.strftime(format).strip
      time << " - "
      time << ends.strftime(format).strip
    end

    private
    def middle_parts(starts, ends)
      return '' if starts.yday == ends.yday
      divider = ' - '

      divider << if starts.month == ends.month
                   ends.strftime('%e').strip
                 else
                   ends.strftime("#{month_and_day(ends)}").strip
                 end
    end

    def month_and_day(datetime)
      if datetime.day > 9
        '%b %e'
      else
        '%b%e'
      end
    end
  end
end
