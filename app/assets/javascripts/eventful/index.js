//= require select2
//= require jquery.timepicker
//= require eventful/datepair
//= require eventful/timepicker

$(function() {
  $('[data-tags]').each(function() {
    $(this).select2({
      tags: $(this).data('tags')
    });
  });

  $('[data-tags][data-value]').each(function() {
    $(this).select2('data', $(this).data('value'));
  });
});
