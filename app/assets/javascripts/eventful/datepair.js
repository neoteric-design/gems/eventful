var datePair = {
	DATEPICKER_FORMAT: 'M dd, yy',
	TIMEPICKER_FORMAT: 'g:i a',
	DATE_FORMAT: 'Y-n-j',

	updateTime: function(target, container) {
		var start    = container.find('input.start_at.timepicker'),
				end      = container.find('input.end_at.timepicker'),
				startInt = start.timepicker('getSecondsFromMidnight');
				endInt   = end.timepicker('getSecondsFromMidnight');

		if (!start.length) {
			return;
		}

		if (target.hasClass('start_at')) {
			end.timepicker('option', 'minTime', startInt);
			if (startInt > endInt) {
				end.timepicker('setTime', startInt + 3600); // +1h
			}
		}
	},
	updateDate: function(target, $container) {
		var $start     = $container.find('input.start_at.datepicker'),
				$end       = $container.find('input.end_at.datepicker'),
				startDate  = new Date($start.val()),
				endDate    = new Date($end.val());

		if (!$start.length || !$end.length) {
			return;
		}

		if (target.hasClass('start_at')) {
			if (startDate.getTime() > endDate.getTime()) {
				$end.val($start.val());
				$end.datepicker('refresh');
				$end.trigger('change');
			}
		} else if (target.hasClass('end_at')) {
			if (startDate.getTime() > endDate.getTime()) {
				$start.val($end.val());
				$start.datepicker('refresh');
				$start.trigger('change');
			}
		}
	},
	update: function() {
		var $this = $(this);

		if (!this.value) {
			return;
		}

		var $container = $this.closest('.datepair');

		if ($this.hasClass('datepicker')) {
			datePair.updateDate($this, $container);

		} else if ($this.hasClass('timepicker')) {
			datePair.updateTime($this, $container);
		}
	}
};
