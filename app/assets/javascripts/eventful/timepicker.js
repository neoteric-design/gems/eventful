var populateDateAndTime = function() {
  var start_date, start_time, end_date, end_time,
      start_field, end_field,
      $datepair = $('.datepair');

  start_field = $($('.start_at').first().data('fill'));
  end_field   = $($('.end_at').first().data('fill'));

  start_date = start_field.data('date');
  start_time = start_field.data('time');

  end_date = end_field.data('date');
  end_time = end_field.data('time');

  $datepair.find('.datepicker.start_at').val(start_date);
  $datepair.find('.timepicker.start_at').val(start_time);
  $datepair.find('.datepicker.end_at').val(end_date);
  $datepair.find('.timepicker.end_at').val(end_time);
};

$(function() {
  populateDateAndTime();

  $('.datepair input.datepicker').each(function() {
    var $this = $(this);

    $this.datepicker({
      dateFormat: datePair.DATEPICKER_FORMAT
    });
    $this.on('change', datePair.update);
  });

  $('.datepair input.timepicker').each(function() {
    var $this = $(this),
        minTime = null;

    if ($this.hasClass('end_at')) {
      minTime = $('.datepair input.timepicker.start_at').val();
    }

    $this.timepicker({
      showDuration: true,
      timeFormat: datePair.TIMEPICKER_FORMAT,
      scrollDefaultNow: true,
      minTime: minTime
    });

    $this.on('change', datePair.update);
  });

});

$(document).on('change', '.datepair > input', function(e) {
  var $fill = $($(this).data('fill')),
      date, time;

  if ($(this).hasClass('start_at')) {
    date = $('.datepair').find('.datepicker.start_at').val();
    time = $('.datepair').find('.timepicker.start_at').val();
  } else {
    date = $('.datepair').find('.datepicker.end_at').val();
    time = $('.datepair').find('.timepicker.end_at').val();
  }

  $fill.val(date + ' ' + time);
});
