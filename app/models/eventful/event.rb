module Eventful
  class Event < ApplicationRecord
    include Model
    include Schedulable
    extend DateScopes
    extend FriendlyId

    self.table_name = 'eventful_events'

    friendly_id :title, use: :slugged
    paginates_per 10
    pubdraft
  end
end
