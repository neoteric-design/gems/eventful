class CreateEventfulSchedules < ActiveRecord::Migration[4.2]
  def change
    create_table :eventful_schedules do |t|
      t.references :schedulable, polymorphic: true
      t.text :program

      t.timestamps null: false
    end
  end
end
