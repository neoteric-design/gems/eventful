module Eventful
  module Schedulable
    def self.included(base)
      base.extend ClassMethods
      base.include InstanceMethods

      base.has_many :schedules, as: :schedulable, dependent: :destroy
      base.accepts_nested_attributes_for :schedules, allow_destroy: true
      base.scope :with_schedules, -> { includes(:schedules) }
      base.before_save :refresh_times
    end

    module ClassMethods
    end

    module InstanceMethods
      def occurrences
        schedules
          .map(&:occurrences)
          .flatten
          .sort_by(&:start_time)
      end

      def formatted_occurrences(format = '%A, %b %e %I:%M%p')
        occurrences.map do |occurrence|
          "#{occurrence.start_time.strftime(format)} - #{occurrence.end_time.strftime(format)}"
        end
      end

      def refresh_times
        return unless schedules.any?
        self[:starts_at] = occurrences.first&.start_time
        self[:ends_at] = occurrences.map(&:end_time).max
      end
    end
  end
end
