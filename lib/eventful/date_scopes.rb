module Eventful
  # ActiveRecord scopes for date ranges
  module DateScopes
    def starts_field
      :starts_at
    end

    def ends_field
      :ends_at
    end

    def past
      where("#{ends_field} < ?", Time.current)
    end

    def future
      where("#{starts_field} > ?", Time.current)
    end

    def current
      where("#{starts_field} <= ?", Time.current)
        .where("#{ends_field} >= ?", Time.current)
    end

    def starts_in(range)
      where(starts_field => range)
    end

    def by_year(year)
      date = Time.zone.local(year, 1, 1)
      starts_in date..date.end_of_year
    end

    def by_month(year, month)
      date = Time.zone.local(year, month, 1)
      starts_in date..date.end_of_month
    end

    def by_day(*args)
      date = args.first.try(:to_time)
      date ||= Time.zone.local(*args)
      starts_in date.beginning_of_day..date.end_of_day
    end
  end
end
