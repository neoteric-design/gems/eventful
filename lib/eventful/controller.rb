module Eventful
  # Mixin for creating Eventful controllers
  module Controller
    def index
      instance_variable_set("@#{collection_name}", find_collection)
    end

    def tagged
      instance_variable_set("@#{collection_name}", find_tagged_collection)

      render :index
    end

    def show
      instance_variable_set("@#{resource_name}", find_resource)

      respond_to do |format|
        format.html
        format.ics
      end
    end

    private

    def resource_class
      controller_name.singularize.camelize.constantize
    end

    def resource_name
      ActiveModel::Naming.singular(resource_class)
    end

    def collection_name
      ActiveModel::Naming.plural(resource_class)
    end

    def base_collection
      resource_class.published.page(params[:page])
    end

    def find_resource
      resource_class.published.friendly.find(params[:id])
    end

    def find_collection
      base_collection.upcoming
    end

    def find_tagged_collection
      base_collection.tagged_with(params[:tag_name])
    end
  end
end
