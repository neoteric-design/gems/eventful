module Eventful
  module Model
    def self.inherited(base)
      base.extend ClassMethods
      base.include InstanceMethods

      base.default_scope -> { order(:starts_at) }
    end

    module ClassMethods

    end

    module InstanceMethods

    end
  end
end
