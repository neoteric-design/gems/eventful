module Eventful
  class Configuration
    def initialize
      yield(self) if block_given?
    end
  end
end
