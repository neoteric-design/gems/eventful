module Eventful
  class RecurringTerminationValidator < ActiveModel::Validator
    def validate(record)
      if record.recurring? && !record.terminating?
        record.errors[:recur_until] << I18n.t('eventful.errors.recurring_termination')
      end
    end
  end

  class EndAfterStartValidator < ActiveModel::Validator
    def validate(record)
      unless record.end_time.present? && record.start_time.present? && record.end_time > record.start_time
        record.errors[:end_time] << I18n.t('eventful.errors.end_after_start')
      end
    end
  end
end