module Eventful
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :model_name, :type => :string, :default => "Event"

    def copy_templates
      template "show.html.erb",            "app/views/#{pluralized_model_name}/show.html.erb"
      template "show.ics.erb",             "app/views/#{pluralized_model_name}/show.ics.erb"
      template "_clip.html.erb",           "app/views/#{pluralized_model_name}/_clip.html.erb"
      template "_sidebar.html.erb",        "app/views/#{pluralized_model_name}/_sidebar.html.erb"
      template "_tag_cloud.html.erb",      "app/views/#{pluralized_model_name}/_tag_cloud.html.erb"
      template "index.html.erb",           "app/views/#{pluralized_model_name}/index.html.erb"
      template "events_controller.rb.erb", "app/controllers/#{pluralized_model_name}_controller.rb"
      template  "event.rb.erb",            "app/models/#{model_name.underscore}.rb"

      copy_file "initializer.rb",       "config/initializers/eventful.rb"
    end

    def setup_routes
      route <<-ROUTES
resources :#{pluralized_model_name}, :only => [:index, :show] do
    collection do
      get 'tagged/:tag_name' => '#{pluralized_model_name}#tagged', :as => :tagged
    end
  end
ROUTES
    end

    def generate_taggable
      generate("acts_as_taggable_on:migration")
    end

    def create_migrations
      rake("railties:install:migrations FROM=eventful")
    end

    private
    def pluralized_model_name
      model_name.pluralize.underscore
    end
  end
end
