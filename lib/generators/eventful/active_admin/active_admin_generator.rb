module Eventful
  class ActiveAdminGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :model_name, :type => :string, :default => 'Event'

    def guard_against_no_activeadmin
      begin
        ActiveAdmin
      rescue
        say_status("quitting", "You must install ActiveAdmin", :red)
        exit
      end

      begin
        File.binread("app/assets/javascripts/active_admin.js")
        File.binread("app/assets/stylesheets/active_admin.css.scss")
      rescue
        say_status("quitting",
                   "You must run the ActiveAdmin install generator",
                   :red)
        exit
      end
    end

    def copy_templates
      template "admin_events.rb.erb",
               "app/admin/#{pluralized_model_name}.rb"

      template "admin_tags.rb.erb",
               "app/admin/#{pluralized_model_name}/categories.rb"
    end

    def config_activeadmin
      admin_ui_line = "# config.admin_ui = :active_admin"

      gsub_file 'config/initializers/eventful.rb',
                /#{Regexp.escape(admin_ui_line)}/mi do |match|
        admin_ui_line.gsub('# ', '')
      end
    end

    def append_assets
      js_path  = "app/assets/javascripts/active_admin.js"
      css_path = "app/assets/stylesheets/active_admin.css.scss"

      # in reverse order because
      # they install above each other
      require_asset('eventful', :js  => js_path, :css => css_path)
      # require_asset('montage',  :js  => js_path, :css => css_path)
      require_asset('compass',  :css => css_path)
    end

    private
    def pluralized_model_name
      @model_name.pluralize.underscore
    end

    def require_asset(asset_name, file_paths = {})
      unless file_paths[:js].blank?
        original_js  = File.binread(file_paths[:js])

        if original_js.include?("require #{asset_name}")
           say_status("skipped",
                      "require #{asset_name} in #{file_paths[:js]}",
                      :yellow)
        else
          insert_into_file(file_paths[:js],
              :after => %r{//= require +['"]?active_admin/base['"]?}) do
            "\n//= require #{asset_name}\n\n"
          end
        end
      end

      unless file_paths[:css].blank?
        original_css = File.binread(file_paths[:css])

        if original_css.include?(%Q{@import "#{asset_name}";})
           say_status("skipped",
                      "import #{asset_name} in #{file_paths[:css]}",
                      :yellow)
        else
          insert_into_file(file_paths[:css],
              :after => %Q(@import "active_admin/base";)) do
            "\n@import \"#{asset_name}\";\n\n"
          end
        end
      end
    end
  end
end
