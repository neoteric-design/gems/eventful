require 'friendly_id'
require 'select2-rails'
require 'pubdraft'
require 'kaminari'
require 'ice_cube'

require 'eventful/configuration'
require 'eventful/controller'
require 'eventful/model'
require 'eventful/engine'
require 'eventful/validators'
require 'eventful/date_scopes'
require 'eventful/schedulable'

module Eventful
  def self.config
    @config ||= Configuration.new
  end

  def self.configure
    yield(config)
 end

  def self.params
    [:title, :body, :starts_at, :ends_at,
     schedules_attributes: [:start_time, :end_time, :recur_until,
                            { days_of_week: [] }, :id, :_destroy]]
  end
end
