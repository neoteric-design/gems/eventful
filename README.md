# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


-------------------
-
# Eventful

This is the basic Event Calendar Rails Engine by Neoteric Design, Inc.

# Installation

```ruby
# Gemfile
gem 'eventful', '~> 0.0.8'
```

```
$ bundle install

$ rails g eventful:install
-or-
$ rails g eventful:install MODEL_NAME (default is Event)

$ rake db:migrate db:test:prepare
```

# Usage

Out of the box, you get an Event model and an EventsController configured to use `eventful`

```ruby
class Event < Eventful::Event
  # Any model can do this, and doesn't need a table created for it
end

class EventsController < ApplicationController
  eventful
  # Any controller can do this
end
```

```ruby
@event = Event.find('slug or id')

@event.title      #=> 'The event title string field'
                  #   also used as the FriendlyID slug

@event.tag_list   #=> Using ActsAsTaggable
                  #   called `categories` in the Admin UI
                  #   but still referred to as `tags` in code

@event.start_at   #=> Fri May 10, 2013 11:32 (datetime field)
@event.end_at     #=> Fri May 10, 2013 12:32 (datetime field)
@event.body       #=> '<html> supported text field'

@event.image_urls #=> utilizing the NeotericUploader gem (please see docs)
@event.images     #=> utilizing the NeotericUploader gem (please see docs)
@event.key_image  #=> utilizing the NeotericUploader gem (please see docs)

Event.by_year(2013)      #=> All events starting in 2013
Event.by_month(9, 2013)  #=> All events strating in Sept 2013

Event.by_day(1, 3, 2013) #=> All events starting on Mar 1, 2013
-or, use a Date object-
Event.by_day(Date.today) #=> All events starting today
```

# View Helpers

```ruby
event_key_image(eventful_object, size = :thumb)
  # returns an image_tag of the key_image if the key_image exists
  # because Rails image_tag returns broken images
  # this helper returns nil otherwise
  # Other size options include :medium and :original

date_range(eventful_object)
  # returns a human-readable date range
  # between the start/end times

time_range(eventful_object)
  # returns a human-readable time range
  # between the start/end times
```

# Eventful Controller

```ruby
class AnyNameController < ApplicationController
  eventful # any controller can do this
end

# provides 3 actions (for now!)
# event_class is discovered based on controller_name
# the instance variables are named afer the event_class
# these examples assume EventsController

def index
  @events = event_class.upcoming
end

def tagged
  @events = event_class.tagged_with(params[:tag_name])
  render :template => 'events/index'
end

def show
  @event = event_class.find(params[:id])
end
```

# ActiveAdmin Integration

```
$ rails g eventful:active_admin
-or-
$ rails g eventful:active_admin MODEL_NAME (default is Event)

--- Installs ActiveAdmin Panels for Events and Categories
```

```ruby
# this example is assuming the
# default MODEL_NAME `Event` during generate

# app/admin/events.rb
ActiveAdmin.register Event do
  eventful_activeadmin
end

# app/admin/events/categories.rb
ActiveAdmin.register Eventful::EventTag, :as => 'category' do
  menu :parent => 'Event',
       :label  => 'Categories'

  # ...
end
```

# Editable labels

```yml
# Key Image
# Update the title and specs for the uploader

en:
  model_name: # for example, 'event'
    key_image:
      title: Key Image
      specs: 100x350
```
