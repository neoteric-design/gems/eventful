$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'eventful/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'eventful'
  s.version     = Eventful::VERSION
  s.authors     = ['Joe Sak', 'Madeline Cowie']
  s.email       = ['joe@joesak.com', 'madeline@cowie.me']
  s.homepage    = 'https://neotericdesign.com'
  s.summary     = 'Basic events calendar Rails engine'
  s.description = 'Read the summary'

  s.files = Dir['{app,config,db,lib,vendor}/**/*'] + ['Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'friendly_id', '>= 5.0.4'
  s.add_dependency 'ice_cube', '>= 0.16'
  s.add_dependency 'kaminari'
  s.add_dependency 'pubdraft',            '>= 1.1'
  s.add_dependency 'rails',               '>= 4.2'
  s.add_dependency 'select2-rails',       '>= 4'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'launchy'
  s.add_development_dependency 'rspec-rails', '>= 3.7.0'
  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'timecop'
end
