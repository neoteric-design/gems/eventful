require 'spec_helper'

module Eventful
  RSpec.describe Schedule, type: :model do
    let(:schedule) { Schedule.new start_time: some_date, end_time: some_date + 3.hours }
    let(:some_date) { Time.new(2016, 2, 20, 11, 0, 0) }

    it { is_expected.to respond_to :schedulable }
    it { is_expected.to respond_to :start_time, :end_time, :recur_until, :days_of_week }

    it 'is initialized with an IceCube::Schedule' do
      expect(Schedule.new.program).to be_a(IceCube::Schedule)
    end

    it 'has virtual attributes for schedule start time and end time' do
      schedule.rebuild_program!

      expect(schedule.program.start_time).to eq(some_date)
      expect(schedule.program.end_time).to eq(some_date + 3.hours)
    end

    it 'must end after it starts' do
      schedule.end_time = some_date - 3.hours

      expect(schedule.valid?).to be_falsey
      expect(schedule.errors.keys).to include(:end_time)
    end

    describe 'recurrence' do
      it 'recurring events must have an ending' do
        schedule.days_of_week = [1,3,5]

        expect(schedule.valid?).to be_falsey

        schedule.recur_until = some_date + 8.weeks
        expect(schedule.valid?).to be_truthy
      end

      it 'can list out all of the occurences in its schedule' do
        schedule.days_of_week = [1]
        schedule.recur_until = some_date + 5.weeks

        schedule.rebuild_program!

        expect(schedule.occurrences.count).to eq(5)
      end

      it 'can set and retrieve an until date for its rules' do
        schedule.days_of_week = [3,4,5]
        schedule.recur_until = some_date + 1.month
        schedule.rebuild_program!

        expect(schedule.program.last.end_time).to be <= some_date + 1.month

        expect(schedule.recur_until).to eq(some_date + 1.month)
      end

      it 'can set and retrieve days of the week to repeat in its schedule' do
        schedule.days_of_week = [3,4,5]
        schedule.recur_until = some_date + 1.month

        schedule.rebuild_program!

        expect(schedule.days_of_week).to eq([3,4,5])
      end

      it 'can have recur_until and days_of_week specified at initialize' do
         schedule = Schedule.create(start_time: "2016-02-29 12:53 PM",
                                    end_time: "2016-02-29 04:00 PM",
                                    days_of_week: ['0'],
                                    recur_until: '2016-03-22')

         expect(schedule).to be_valid
         expect(schedule).to be_persisted
      end
    end
  end
end