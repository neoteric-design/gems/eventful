require 'timecop'
require 'spec_helper'

module Eventful
  RSpec.describe Event, type: :model do
    describe 'schedules' do
      let(:start_time) { Time.local(2017, 3, 1, 15, 0) }
      let(:end_time) { Time.local(2017, 3, 1, 17, 0) }
      let(:end_day) { Time.local(2017, 3, 29) }

      # Recurs first 4 wednesdays of March 2017; last on the 22th
      let(:schedule_1) { Schedule.new(start_time: start_time, end_time: end_time, recur_until: end_day - 2.days, days_of_week: [3] )}
      # Recurs every saturday of March 2017; 4 times, last on the 25th
      let(:schedule_2) { Schedule.new(start_time: start_time + 3.days, end_time: end_time + 3.days, recur_until: end_day, days_of_week: [6] )}
      let(:event) { Event.create!(schedules: [schedule_1, schedule_2]) }

      describe 'caching time range' do
        it 'starts_at caches the start time of the first chronological occurrence' do
          expect(event.starts_at).to eq(start_time)
        end

        it 'ends_at caches the end time of the last chronological occurrence' do
          expect(event.ends_at).to eq(Time.local(2017, 3, 25, 18, 0))
        end
      end
    end
    describe "scopes" do
      let!(:now) { Time.current }

      let!(:right_now) do
        Event.create!(starts_at: now, ends_at: now.tomorrow)
      end

      let!(:future) do
        Event.create!(starts_at: Time.current.tomorrow)
      end

      let!(:current) do
        Event.create!(starts_at: Time.current.advance(hours: -1),
                      ends_at: Time.current.tomorrow)
      end

      let!(:past) do
        Event.create!(ends_at: Time.current.yesterday)
      end

      let!(:aug_2010) do
        Event.create!(starts_at: Time.local(2010, 8, 1))
      end

      let!(:jul_2010) do
        Event.create!(starts_at: Time.local(2010, 7, 1))
      end

      let!(:year_2011) do
        Event.create!(starts_at: Time.local(2011, 3, 1))
      end

      let!(:day_nov_11_2012) do
        Event.create!(starts_at: Time.local(2012, 11, 11))
      end

      describe ".by_year" do
        it "returns events in the given year" do
          expect(Event.by_year(2011)).to include(year_2011)
        end

        it "does not return events in other years" do
          expect(Event.by_year(2010)).to_not include(year_2011)
        end
      end

      describe ".by_month" do
        it "returns events in the given month" do
          expect(Event.by_month(2010, 8)).to include(aug_2010)
        end

        it "does not return events in other months" do
          expect(Event.by_month(2010, 8)).to_not include(jul_2010)
        end
      end

      describe ".by_day" do
        it "returns events on the given day" do
          expect(Event.by_day(2012, 11, 11)).to include(day_nov_11_2012)
        end

        it "accepts a date object as an argument" do
          day_in_nov = Time.local(2012, 11, 11)

          expect(Event.by_day(day_in_nov)).to include(day_nov_11_2012)

          # check Today for good measure
          expect(Event.by_day(Time.current)).to include(right_now)
        end

        it "does not return events on other days" do
          expect(Event.by_day(2012, 11, 8)).to_not include(day_nov_11_2012)
        end
      end

      describe ".future" do
        it "returns events starting later" do
          expect(Event.future).to include(future)
        end

        it "does not return events that already started" do
          expect(Event.future).to_not include(current)
        end

        it "does not return events that already ended" do
          expect(Event.future).to_not include(past)
        end
      end

      describe ".current" do
        it "returns events starting right now" do
          Timecop.freeze(now) do
            expect(Event.current).to include(right_now)
          end
        end

        it "returns events that started but haven't ended" do
          expect(Event.current).to include(current)
        end

        it "does not return events that already ended" do
          expect(Event.current).to_not include(past)
        end

        it "does not return future events" do
          expect(Event.current).to_not include(future)
        end
      end

      describe ".past" do
        it "returns events that have already ended" do
          expect(Event.past).to include(past)
        end

        it "does not return events that are current" do
          expect(Event.past).to_not include(current)
        end

        it "does not return events that are future" do
          expect(Event.past).to_not include(future)
        end
      end
    end
  end
end
