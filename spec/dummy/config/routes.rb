Rails.application.routes.draw do
  # mount ::Montage::Engine => '/montage'

  resources :events, :only => [:index, :show] do
    collection do
      get 'tagged/:tag_name' => 'events#tagged', :as => :tagged
    end
  end


end
