# This migration comes from eventful (originally 1)
class CreateEventfulEvents < ActiveRecord::Migration[4.2]
  def change
    create_table :eventful_events do |t|
      t.string   :title
      t.datetime :starts_at, index: true
      t.datetime :ends_at, index: true
      t.text     :body
      t.string   :type, index: true
      t.string   :state
      t.string   :slug, index: true

      t.timestamps null: false
    end
  end
end
