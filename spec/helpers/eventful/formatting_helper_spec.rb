require 'spec_helper'

module Eventful
  RSpec.describe FormattingHelper, type: :helper do
    describe "#date_range" do
      it "silently allows nils" do
        event = OpenStruct.new(:starts_at => nil,
                               :ends_at   => nil)
        expect(helper.date_range(event)).to be_nil
      end

      it "shows the start date for single day events" do
        starts = Time.local(2013, 1, 1, 13, 0)
        ends   = Time.local(2013, 1, 1, 14, 0)
        event  = OpenStruct.new(:starts_at => starts,
                                :ends_at   => ends)

        result = helper.date_range(event)
        expect(result).to eq("Tuesday, Jan 1, 2013")
      end

      it "shows the start day through end day for same month events" do
        starts = Time.local(2013, 1, 1, 13, 0)
        ends   = Time.local(2013, 1, 3, 14, 0)
        event  = OpenStruct.new(:starts_at => starts,
                                :ends_at   => ends)

        result = helper.date_range(event)
        expect(result).to eq("Tuesday, Jan 1 - 3, 2013")
      end

      it "shows the start month and day through end month and day for same year events" do
        starts = Time.local(2013, 1, 29, 13, 0)
        ends   = Time.local(2013, 2, 3, 14, 0)
        event  = OpenStruct.new(:starts_at => starts,
                                :ends_at   => ends)

        result = helper.date_range(event)
        expect(result).to eq("Tuesday, Jan 29 - Feb 3, 2013")
      end

      it "shows the start month day through end month and day for different year events" do
        starts = Time.local(2013, 12, 29, 13, 0)
        ends   = Time.local(2014, 1, 3, 14, 0)
        event  = OpenStruct.new(:starts_at => starts,
                                :ends_at   => ends)

        result = helper.date_range(event)
        expect(result).to eq("Sunday, Dec 29 - Jan 3, 2014")
      end
    end

    describe "#time_range" do
      it "silently allows nils" do
        event = OpenStruct.new(:starts_at => nil,
                               :ends_at   => nil)
        expect(helper.time_range(event)).to be_nil
      end

      it "shows the start and end time" do
        starts = Time.local(2013, 1, 1, 13, 0)
        ends   = Time.local(2013, 1, 1, 14, 0)
        event  = OpenStruct.new(:starts_at => starts,
                                :ends_at   => ends)

        result = helper.time_range(event)
        expect(result).to eq("1:00pm - 2:00pm")
      end
    end
  end
end
